<?php
	session_start();
	header('Content-Type: text/html; charset=utf-8');
	//exit();
	//require_once("funciones_logica.php"); 	
	require_once("funciones_API.php"); 	
	require_once("funciones_consultasBPRO.php");
	
	//$funciones 		  = new funciones_logica();
	$interaccion_api  = new funciones_API();
	$interaccion_BPRO = new funciones_consultasBPRO();
?>

<!DOCTYPE html>
<link href="tabla4.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="jquery-1.8.2.min.js"></script>
<title>Actualiza APP CDA </title>
<h2>Actualiza APP CDA - Vins STATUS_REGISTER=False (no adoptados) con cita por confirmar [ Status - 1 Cita ]</h2>

<?php

	set_time_limit(0);
	//----------------------------------------------------------------------------
	$registrados_en_CDA = "False";
	$pagina = 1;

	//aquí se interroga al json solamente para obtener filas totales y filas por página para poder hacer el total de páginas
	//$respuesta = consulta_vins_registrados( $pagina, $registrados_en_CDA );	
	//IMP $respuesta = consulta_vins_con_cita_en_1($pagina, $registrados_en_CDA);	//Esta consulta devuelve todas las citas con el fitro de "status_register" en false y "status" de cita
	
	$respuesta = $interaccion_api->consulta_vins_con_cita_en_1($pagina, $registrados_en_CDA);
	$vins_con_cita_en_1 = json_decode($respuesta,true);

	echo "Filas totales:    ".$vins_con_cita_en_1["pagination"]["total_rows"]."<br>";
	echo "Filas por página: ".$vins_con_cita_en_1["pagination"]["per_page"]."<br>";
		
	$total_paginas = total_paginas($vins_con_cita_en_1["pagination"]["total_rows"], $vins_con_cita_en_1["pagination"]["per_page"]);

	echo "<br>
			<table border=1>
				<th>#</th>
			  	<th>VIN</th>
			  	<th>Id cita</th>
			  	<th>Sucursal</th>
			  	<th>Suc-bodyshops</th>
			  	<th>BodyShop</th>";
	
			$kontador = 0;
			$arregloNube = array();
			$arregloTotal = array();
			$vinsTotales = "";
	
			for($kuenta_pagina = 1; $kuenta_pagina <= $total_paginas; $kuenta_pagina++ ){
				
				//$respuesta = consulta_vins_con_cita_en_1( $kuenta_pagina, $registrados_en_CDA );
				
				$respuesta = $interaccion_api->consulta_vins_con_cita_en_1( $kuenta_pagina, $registrados_en_CDA );
				$vins_con_cita_en_1 = json_decode($respuesta,true);

				// ============================================================================
				$kontador_citas = 1;			
					
					for($i = 0; $i < count($vins_con_cita_en_1["data"]); $i++){						
						
						$vins_no_registrados[] = $vins_con_cita_en_1["data"][$i]["vin"];
						$cadena_vins_no_reg .= "'".$vins_con_cita_en_1["data"][$i]["vin"]."',";
				
						$VIN = $vins_con_cita_en_1["data"][$i]["vin"];				
				
						// asignación de datos de información básica nube
						$arregloNube[$kontador]['VIN'] 				 	= $VIN;
						$arregloNube[$kontador]['Base_adopto'] 		 	= '';
						$arregloNube[$kontador]['nombre_asesor'] 	 	= '';				
						$arregloNube[$kontador]['Fecha_cita_tierra'] 	= date("Y-m-d H:i:s");
						$arregloNube[$kontador]['Id_vehiculo_nube']  	= $vins_con_cita_en_1["data"][$i]["vehicle"]["vehicle"];
						$arregloNube[$kontador]['Id_propietario_nube'] 	= $vins_con_cita_en_1['data'][$i]['vehicle']['user']['id'];
						$arregloNube[$kontador]['Id_solicitud'] 		= $vins_con_cita_en_1['data'][$i]['schedule'];
						$arregloNube[$kontador]['concuerda'] 			= false;
						$arregloNube[$kontador]['contador'] 			= 1;														

				
						if($vins_con_cita_en_1['data'][$i]['bodyshops'] != ''){
							$arregloNube[$kontador]['id_body_nube'] = $vins_con_cita_en_1['data'][$i]['bodyshops']['bodyshops'];
						}
						elseif ($vins_con_cita_en_1['data'][$i]['sucursal'] != '') {
							$arregloNube[$kontador]['id_sucursal_nube'] = $vins_con_cita_en_1['data'][$i]['sucursal']['sucursal'];
						}
		
						echo "<tr>
								<td>".$kontador."</td>
								<td>".$vins_con_cita_en_1["data"][$i]["vin"]."</td>
								<td>".$vins_con_cita_en_1["data"][$i]["schedule"]."</td>
								<td>".$vins_con_cita_en_1["data"][$i]["sucursal"]["sucursal"]." - ".$vins_con_cita_en_1["data"][$i]["sucursal"]["name"]."</td>
								<td>".$vins_con_cita_en_1["data"][$i]["sucursal"]["bodyshops"]."</td>
								<td>".$vins_con_cita_en_1["data"][$i]["bodyshops"]["bodyshops"]." - ".$vins_con_cita_en_1["data"][$i]["bodyshops"]["name"]."</td>
							  </tr>";		
				
						$kontador++;							
					}
				$kontador_citas++;
			}
			
			echo "</table>";	
			$cadena_vins_no_reg = rtrim($cadena_vins_no_reg, ", ");
			echo "<br><div style='overflow-wrap: break-word;'>".$cadena_vins_no_reg."</div><hr>";
		
//---------------------------  BLOQUE DE CODIGO DONDE CONSULTAMOS LAS CITAS DE TIERRA ----------------------------------------------------------------------------

		//IMPinclude("conServer_mssql.php");
		//IMP$conex_CDAP = new conServer_mssql('', "CDAP");

		$hora_inicio = date("Y-m-d H:i:s");
		
		echo "<p style='font-weight:bold ;color:red'> Inicio: ".$hora_inicio."</p>";

		/*IMP $sql_CITAS_CDAP = "SELECT 
					  	appcitas.CAMBIO_ID
						,appcitas.CIT_IDCITA
						,appcitas.CIT_IDORDEN
						,appcitas.CIT_NUMSERIE
						,appcitas.CIT_FECCITA
						,appcitas.CIT_HORCITA
						,appcitas.CIT_IDSTATUSCIT
						,appcitas.CIT_IDASESOR
				 		,appcitas.BASE
				 		,appcitas.INSERTADO_EN
				 		,appcitas.OPERACION
						,visCIT.ASESOR
						,visCIT.MARCA
						,visCIT.CIT_ANMODELO
						,visCIT.cit_kilometraje KILOMETRAJE
						,visCIT.CODIGO_EXTERIOR COLOR_EXT
				 		,appcitas.REVISADO_SI_NO 
				 		,appcitas.REVISADO_ULTIMAVEZ
				 		,appcitas.ACTUALIZADO_EN_APP
				 		,appcitas.FECHA_ACTUALIZADO_APP
				 		,app_VINS_ADOPTADOS.VIN VIN_ADOPTADO
				 		,app_SUC_TIERRA_NUBE.ID_SUC_NUBE
				 		,app_SUC_TIERRA_NUBE.ID_BODY_NUBE
						FROM Bpro_ConectaBase.dbo.app_CITAS_CAMBIOS appcitas
				  		LEFT JOIN Bpro_ConectaBase.dbo.visSER_CITASASESORES visCIT ON appcitas.CIT_IDCITA = visCIT.CIT_IDCITA AND BASE = visCIT.NOMBRE_BASE
				  		LEFT JOIN app_VINS_ADOPTADOS ON appcitas.CIT_NUMSERIE = app_VINS_ADOPTADOS.VIN
				  		LEFT JOIN app_SUC_TIERRA_NUBE ON appcitas.BASE = app_SUC_TIERRA_NUBE.BASE
						WHERE 1=1
						AND appcitas.CIT_NUMSERIE != ''
						AND appcitas.OPERACION = 'INS' AND ( appcitas.CIT_IDSTATUSCIT = 'ING' ) -- OR CIT_IDSTATUSCIT = 'NRE' )
						AND CONVERT(DATE,appcitas.CIT_FECCITA,103) >= CONVERT(DATE,GETDATE())
							-- AND SUBSTRING( CIT_IDORDEN, 1,1 ) = 'G'
						AND appcitas.CIT_NUMSERIE
						IN (".$cadena_vins_no_reg.")
						--AND app_VINS_ADOPTADOS.VIN IS NULL
						ORDER BY CAMBIO_ID ASC  
						-- el cambio más viejo se refleja antes que el cambio más nuevo, por eso es ASC
						-- CONVERT(DATETIME,appcitas.CIT_FECCITA+' '+appcitas.CIT_HORCITA,103) DESC
						";*/
		
		//IMP $resultados_citas_CDAP = $conex_CDAP->ejecutarQuerySQL($sql_CITAS_CDAP);		
		$resultados_citas_CDAP = $interaccion_BPRO->GetDatosCita_BPRO($cadena_vins_no_reg);
		echo "<br>";						
		$k= 0;

		echo "<table border=1>
					<th>#</th>
					<th>Id<br>cita</th>
					<th>Id<br>Orden</th>
					<th>Serie</th>
		      		<th>Fecha<br>cita</th>
		      		<th>Hora<br>cita</th>
		      		<th>Id<br>status</th>
		      		<th>Id<br>Asesor</th>
			  		<th>Base</th>
			  		<th>Insertado<br>en</th>
			  		<th>Operación</th>
			  		<th>Asesor</th>
			  		<th>Marca</th>
			  		<th>Modelo</th>
			  		<th>Kilometraje</th>
			  		<th>Color<br>Ext</th>
			  		<th>Revisado<br>S/N</th>
			  		<th>Última<br>revisión</th>
			  		<th>Actualizado<br>en APP</th>
			  		<th>Fecha<br>actualizado<br>en APP</th>
			  		<th>VIN_ADOPTADO</th>
			  		<th>SUC_NUBE</th>
			  		<th>BODY_NUBE</th>";
			  		
			if($resultados_citas_CDAP!=null){
				$arregloConCitaCDA = array();		
				foreach($resultados_citas_CDAP as $fila_citas_CDAP){
								
					//Si Ya tiene cita en BPro agrego en que base se asigno
					
					if(!array_key_exists($fila_citas_CDAP['CIT_NUMSERIE'], $arregloConCitaCDA)){
						
						$arregloConCitaCDA[$fila_citas_CDAP['CIT_NUMSERIE']]['CIT_NUMSERIE'] 	= $fila_citas_CDAP['CIT_NUMSERIE'];
						$arregloConCitaCDA[$fila_citas_CDAP['CIT_NUMSERIE']]['CAMBIO_ID'] 		= $fila_citas_CDAP['CAMBIO_ID'];						
						$arregloConCitaCDA[$fila_citas_CDAP['CIT_NUMSERIE']]['CIT_IDCITA'] 		= $fila_citas_CDAP['CIT_IDCITA'];
						$arregloConCitaCDA[$fila_citas_CDAP['CIT_NUMSERIE']]['CIT_FECCITA'] 	= $fila_citas_CDAP['CIT_FECCITA'];
						$arregloConCitaCDA[$fila_citas_CDAP['CIT_NUMSERIE']]['CIT_HORCITA'] 	= $fila_citas_CDAP['CIT_HORCITA'];						
						$arregloConCitaCDA[$fila_citas_CDAP['CIT_NUMSERIE']]['BASE'] 			= $fila_citas_CDAP['BASE'];
						$arregloConCitaCDA[$fila_citas_CDAP['CIT_NUMSERIE']]['ID_BODY_NUBE'] 	= $fila_citas_CDAP['ID_BODY_NUBE'];
						$arregloConCitaCDA[$fila_citas_CDAP['CIT_NUMSERIE']]['ID_SUC_NUBE'] 	= $fila_citas_CDAP['ID_SUC_NUBE'];
						$arregloConCitaCDA[$fila_citas_CDAP['CIT_NUMSERIE']]['MARCA'] 			= ucfirst(strtolower($fila_citas_CDAP["MARCA"]));
						$arregloConCitaCDA[$fila_citas_CDAP['CIT_NUMSERIE']]['CIT_ANMODELO'] 	= $fila_citas_CDAP['CIT_ANMODELO'];
						$arregloConCitaCDA[$fila_citas_CDAP['CIT_NUMSERIE']]['COLOR_EXT'] 		= utf8_encode($fila_citas_CDAP["COLOR_EXT"]);
						$arregloConCitaCDA[$fila_citas_CDAP['CIT_NUMSERIE']]['KILOMETRAJE'] 	= $fila_citas_CDAP['KILOMETRAJE'];						
						$arregloConCitaCDA[$fila_citas_CDAP['CIT_NUMSERIE']]['NOMBRE_ASESOR'] 	=  utf8_encode($fila_citas_CDAP["ASESOR"]);


						//ASIGNACION DEL VIN
						$arregloConCitaCDA[$fila_citas_CDAP['CIT_NUMSERIE']]['SE_ADOPTO_VIN'] 	=  ($fila_citas_CDAP["ASESOR"] == '') ? false : true;
					}
				$k++;	

						echo "<tr>";
						echo "<td>".$k."</td>";
						echo "<td>".$fila_citas_CDAP["CIT_IDCITA"]."</td>";
						echo "<td>".$fila_citas_CDAP["CIT_IDORDEN"]."</td>";
						echo "<td>".$fila_citas_CDAP["CIT_NUMSERIE"]."</td>";
						echo "<td>".$fila_citas_CDAP["CIT_FECCITA"]."</td>";
						echo "<td>".$fila_citas_CDAP["CIT_HORCITA"]."</td>";
						echo "<td>".$fila_citas_CDAP["CIT_IDSTATUSCIT"]."</td>";
						echo "<td>".$fila_citas_CDAP["CIT_IDASESOR"]."</td>";
						echo "<td>".$fila_citas_CDAP["BASE"]."</td>";
						echo "<td>".$fila_citas_CDAP["INSERTADO_EN"]."</td>";
						echo "<td>".$fila_citas_CDAP["OPERACION"]."</td>";
						echo "<td>".utf8_encode($fila_citas_CDAP["ASESOR"])."</td>";
						echo "<td>".ucfirst(strtolower($fila_citas_CDAP["MARCA"]))."</td>";
						echo "<td>".$fila_citas_CDAP["CIT_ANMODELO"]."</td>";
						echo "<td>".$fila_citas_CDAP["KILOMETRAJE"]."</td>";
						echo "<td>".utf8_encode($fila_citas_CDAP["COLOR_EXT"])."</td>";
						echo "<td>".$fila_citas_CDAP["REVISADO_SI_NO"]."</td>";
						echo "<td>".$fila_citas_CDAP["REVISADO_ULTIMAVEZ"]."</td>";
						echo "<td>".$fila_citas_CDAP["ACTUALIZADO_EN_APP"]."</td>";
						echo "<td>".$fila_citas_CDAP["ACTUALIZADO_EN_APP"]."</td>";
						echo "<td>".$fila_citas_CDAP["VIN_ADOPTADO"]."</td>";
						echo "<td>".$fila_citas_CDAP["ID_SUC_NUBE"]."</td>";
						echo "<td>".$fila_citas_CDAP["ID_BODY_NUBE"]."</td>";					
						echo "</tr>";
				}
			}
		echo "</table>";

		$contador = 0;
		
		//Compruebo que el query me haya devuelto resultados
		if($resultados_citas_CDAP!=null){
			
			$arregloAux = array();
			
			foreach ($arregloNube as $key => $value) //Recorro el arreglo de la nube
			{																
				
				//echo "Primer indice ".$key."<br>";
				if(array_key_exists($value['VIN'], $arregloConCitaCDA)){
			
					$arregloAux[] = $value['VIN'];
					
					$arregloNube[$key]['Base_adopto'] 			= $arregloConCitaCDA[$value['VIN']]['BASE'];
					// comparativa suc tierra vs suc nube / body tierra vs body nube
					$arregloNube[$key]['id_body_tierra'] 		= $arregloConCitaCDA[$value['VIN']]["ID_BODY_TIERRA"];
					$arregloNube[$key]['id_sucursal_tierra'] 	= $arregloConCitaCDA[$value['VIN']]["ID_SUC_NUBE"]; 					
					
					//PARAMETROS PARA ACTUALIZA VEHICULO
					$arregloNube[$key]['marca'] 				= $arregloConCitaCDA[$value['VIN']]['MARCA'];
					$arregloNube[$key]['modelo'] 				= $arregloConCitaCDA[$value['VIN']]['CIT_ANMODELO'];
					$arregloNube[$key]['color_exterior'] 		= $arregloConCitaCDA[$value['VIN']]['COLOR_EXT'];
					$arregloNube[$key]['kilometraje'] 			= $arregloConCitaCDA[$value['VIN']]['KILOMETRAJE'];

					//PARAMETROS PARA ACTUALIZA CITA
					$arregloNube[$key]['Fecha_cita_tierra'] 	= $arregloConCitaCDA[$value['VIN']]['CIT_FECCITA'];					
					$arregloNube[$key]['hora_cita_tierra'] 		= $arregloConCitaCDA[$value['VIN']]['CIT_HORCITA'];
					$arregloNube[$key]['id_cita_tierra'] 		= $arregloConCitaCDA[$value['VIN']]['CIT_IDCITA'];					
					$arregloNube[$key]['nombre_asesor'] 		= $arregloConCitaCDA[$value['VIN']]['NOMBRE_ASESOR'];					
					$arregloNube[$key]['cambio_id'] 			=  $arregloConCitaCDA[$value['VIN']]['CAMBIO_ID'];
					
					$arregloNube[$key]['concuerda'] 			= "NO";
					
					$id_body_Nube 		= $arregloNube[$key]['id_body_nube'];
					$id_body_Tierra 	= $arregloConCitaCDA[$value['VIN']]["ID_BODY_NUBE"];
					$id_sucursal_Nube 	= $arregloNube[$key]['id_sucursal_nube'];
					$id_sucursa_Tierra 	= $arregloConCitaCDA[$value['VIN']]["ID_SUC_NUBE"];

					
					//la validacion en esta forma, resultaba true, comparabamos la sucursal_nube y sucursal_tierra o en su efecto con un OR body_nube vs body_tierra
					//tenian que cumplir que sean iguales para un SI, pero al tener uno de ellos vacio, la validacion no tenia efecto y se daba como positivo.
					//if($id_body_Nube == $id_body_Tierra || $id_sucursal_Nube == $id_sucursa_Tierra)	 	
					if(($id_body_Nube != '' and $id_body_Tierra != '' and $id_body_Nube == $id_body_Tierra) or ($id_sucursal_Nube != '' and $id_sucursa_Tierra != '' and $id_sucursal_Nube == $id_sucursa_Tierra)){												

							$arregloNube[$key]['concuerda'] = "SI";
					}		
				}
				else{
					unset($arregloNube[$key]);
				}
			}//Fin Foreach
		}
		elseif ($resultados_citas_CDAP == null) {
				$arregloNube = array();
		}
//---------------------------FIN BLOQUE DE CODIGO DONDE CONSULTAMOS LAS CITAS DE TIERRA ----------------------------------------------------------------------------		
		

		/*
			SECCION DONDE LLEVO A CABO EL CONTADOR DE LOS VINS PARA IGNORAR LAS CITAS REPETIDAS QUE COINCIDAN CON ESTAS DOS VALIDACIONES
			1. EL NUMERO DE VIN 
			2. EL LUGAR DE LA CITA (SUCURSAL O BODY)
		*/
		$arregloContador = array(); //Declaro un arreglo auxiliar para llenar y llevar control

		if(!empty($arregloNube)){
			
			foreach ($arregloNube as $indince => $valor) {

				$ValorKey 			= $valor['VIN']; 				//Asigno el VIN del arreglo original que recorro con el foreach a una variable auxiliar, sera mi indice
				$Id_Sucursal_nube 	= $valor['id_sucursal_nube'];	//Tomo el valor del campo id sucursal del arreglo de la nube y lo asigno a una variable auxiliar.
				$Id_Body_nube 		= $valor['id_body_nube']; 		//Tomo el valor del campo id sucursal del arreglo de la nube y lo asigno a una variable auxiliar.

				
				//Cada registro posicionado del arreglo en el foreach lo valido. 
				//La validacion es si existe en el arreglo auxiliar y este concide en su lugar geografico
				//Al cumplirse estas validaciones modifico el "contador" del arreglo NUBE asi como el campo "concuerda" que le pongo "NO" 
				if(isset($arregloContador[$ValorKey]) && $arregloContador[$ValorKey]['SUCURSAL_SUC'] == $Id_Sucursal_nube || 
					isset($arregloContador[$ValorKey]) && $arregloContador[$ValorKey]['SUCURSAL_BOD'] == $Id_Body_nube){
					
					$arregloNube[$indince]['contador'] 		+= 1;
					$arregloNube[$indince]['concuerda'] 	 = "NO, CITA REPETIDA";
					$arregloContador[$ValorKey]['CONTADOR'] +=1;					
				}
				else{			
					$arregloContador[$valor['VIN']]['CONTADOR'] 	= 1;						
					$arregloContador[$valor['VIN']]['SUCURSAL_SUC'] = $valor['id_sucursal_nube'];
					$arregloContador[$valor['VIN']]['SUCURSAL_BOD'] = $valor['id_body_nube'];
				}	
			}
		}
		//FIN DE LA SECCION DEL CONTADOR------------------------------------------------------------------


		echo "<br>";
		$hora_fin = date("Y-m-d H:i:s");
		echo "<p style='font-weight:bold ;color:red'> Fin: ".$hora_fin."</p><hr>";			
		echo "<br>
				
		<h3>Lista del procesamiento info de la nube</h3> <br>";

		//$funciones->funcion_adopta_VIN($arregloNube);
		
		$num = 0;
		echo "<table border='1'>";

			if(!empty($arregloNube)){				
				
				foreach ($arregloNube as $key) {

					if($num == 0){
						echo "
						<th>No</th>
						<th>Folio Cita Nube</th>
						<th>Folio Cita Tierra</th>
						<th>VIN</th>
						<th>Que Base lo adopto</th>
						<th>Fecha Cita Tierra</th>
						<th>Hora Cita Tierra</th>
						<th>Asesor Tierra</th>
						<th>Folio Vehiculo Nube</th>
						<th>Folio Propietario Nube</th>
						<th style='background:red; color:white;'>Folio Sucursal Nube</th>
						<th style='background:red; color:white;'>Folio Sucursal Tierra</th>
						<th style='background:red; color:white;'>Folio Body Nube</th>
						<th style='background:red; color:white;'>Folio Body Tierra</th>
						<th>Coincide Tierra - Nube</th>
						<th>Marca</th>
						<th>Modelo</th>
						<th>Color</th>
						<th>Kilomentraje</th>
						<th>Contador</th>
						<th>Cambio ID</th>";
						
					}
					$num++;
					echo "<tr>
							<td>".$num."</td>
							<td>".$key['Id_solicitud']."</td>
							<td>".$key['id_cita_tierra']."</td>
							<td>".$key['VIN']."</td>
							<td>".$key['Base_adopto']."</td>
							<td>".$key['Fecha_cita_tierra']."</td>
							<td>".$key['hora_cita_tierra']."</td>
							<td>".$key['nombre_asesor']."</td>
							<td>".$key['Id_vehiculo_nube']."</td>
							<td>".$key['Id_propietario_nube']."</td>
							<td>".$key['id_sucursal_nube']."</td>
							<td>".$key['id_sucursal_tierra']."</td>
							<td>".$key['id_body_nube']."</td>		
							<td>".$key['id_body_tierra']."</td>	
							<td>".$key['concuerda']."</td>
							<td>".$key['marca']."</td>
							<td>".$key['modelo']."</td>
							<td>".$key['color_exterior']."</td>
							<td>".$key['kilometraje']."</td>
							<td>".$key['contador']."</td>
							<td>".$key['cambio_id']."</td></tr>";	
	
					if(!is_null($key['concuerda']) && $key['concuerda'] == 'SI'){
						
						$resulP = $interaccion_BPRO->adopta_vin( $key['VIN'], $key['Base_adopto'], $key['Id_propietario_nube'], $key['Id_vehiculo_nube'] );						
						$interaccion_api->actualiza_vehiculo( $key['Id_vehiculo_nube'], $key['marca'], $key['modelo'], $key['color_exterior'], $key['kilometraje'] );					
						$interaccion_api->adopta_cita($key['Id_solicitud'],$key['id_cita_tierra'], $key['hora_cita_tierra'], $key['Fecha_cita_tierra']);						
						$mesja = "Actualice desde paso 1";
						$interaccion_api->mensaje_usuario($key['Id_propietario_nube'], $key['Fecha_cita_tierra'], $key['hora_cita_tierra'],$mesja);                    
                        
                        $interaccion_BPRO->actualiza_app_citas($key['cambio_id']);

                        if($resulP ==  1){
                        	$interaccion_api->actualiza_status_register_a_true($key['Id_vehiculo_nube']);
                        }						                    
					}							
				}//Fin Foreach
			}
			echo "</table>";		

		echo "<br><br>";
		

		//IMP$citas_resultados = ConsultaTierraCitas();
		$citas_resultados = $interaccion_BPRO->ConsultaTierraCitas();
		echo "<br><br>";
		echo "<h3>Lista de las citas que exiten en BPRO</h3> <br>";
		echo "
			<table border='1'>
				<thead>
					<th>No.</th>
					<th>ID Registro</th>
					<th>Id Cita</th>
					<th>Id Orden</th>
					<th>VIN</th>
					<th>Fec Cita</th>
					<th>Hra Cita</th>
					<th>Estatus Cita</th>
					<th>Id Asesor</th>
					<th>Base</th>
					<th>Insertado</th>
					<th>Opera</th>
					<th>Nom asesor</th>
					<th>Marca</th>
					<th>Anio</th>
					<th>KM</th>
					<th>Color</th>
					<th>Revisado</th>
					<th>Revisado Ult</th>
					<th>Actualizado en app</th>
					<th>Fecha Actualiza</th>
					<th>VIN Adoptado</th>
					<th>ID Suc Nube</th>
					<th>ID Body Nube</th>
				</thead>
			
		";
		
		//Sprint_r($citas_resultados[0]);
		$num = 1;

		foreach ($citas_resultados as $key => $value) {	
			
			echo "<tr>
				<td>".$num++."</td>
				<td>".$value['CAMBIO_ID']."</td>
				<td>".$value['CIT_IDCITA']."</td>
				<td>".$value['CIT_IDORDEN']."</td>
				<td>".$value['CIT_NUMSERIE']."</td>
				<td>".$value['CIT_FECCITA']."</td>
				<td>".$value['CIT_HORCITA']."</td>
				<td>".$value['CIT_IDSTATUSCIT']."</td>
				<td>".$value['CIT_IDASESOR']."</td>
				<td>".$value['BASE']."</td>
				<td>".$value['INSERTADO_EN']."</td>
				<td>".$value['OPERACION']."</td>
				<td>".$value['ASESOR']."</td>
				<td>".$value['MARCA']."</td>
				<td>".$value['CIT_ANMODELO']."</td>
				<td>".$value['KILOMETRAJE']."</td>
				<td>".$value['COLOR_EXT']."</td>
				<td>".$value['REVISADO_SI_NO']."</td>
				<td>".$value['REVISADO_ULTIMAVEZ']."</td>
				<td>".$value['ACTUALIZADO_EN_APP']."</td>
				<td>".$value['FECHA_ACTUALIZADO_APP']."</td>
				<td>".$value['VIN_ADOPTADO']."</td>
				<td>".$value['ID_SUC_NUBE']."</td>
				<td>".$value['ID_BODY_NUBE']."</td>
			</tr>";

			$arrayAuxi = array();		

			$array['vin'] 				= $value['CIT_NUMSERIE'];
			$array['hour_schedule'] 	= $value['CIT_HORCITA'];
			$array['data_schedule'] 	= $value['CIT_FECCITA'];
			
			$array['sucursal'] 			= $value['ID_SUC_NUBE'];
			$array['bodyshops']			= $value['ID_BODY_NUBE'];


			$array['folio_CDA'] 		= $value['CIT_IDCITA'];

			//Mnesaje
			$array['asesor_cita']   	= $value['ASESOR'];	

			//Actualiza vehiculo
			$array['marca']				= $value['MARCA'];	
			$array['modelo']			= $value['CIT_ANMODELO'];	
			$array['color_exterior']	= $value['COLOR_EXT'];	
			$array['kilometraje']		= $value['KILOMETRAJE'];	
			
			//Cuando el VIN no esta adoptado, entro
			if($value['VIN_ADOPTADO'] == ''){
				
				//Consulto a la nube por el VIN
				$ExisteVIN  = $interaccion_api->ConsultaVIN($value['CIT_NUMSERIE']);
				//Consulto a la nube por una cita del Folio CDA
				$ExisteCITA = $interaccion_api->ConsultaCita($value['CIT_IDCITA']);

				$ExisteCITA2 = $interaccion_api->ConsultaCitaXVin($value['CIT_NUMSERIE']);			
				
				//Entro por que el VIN existe, pero no hay cita hecha
				if($ExisteVIN != false && !$ExisteCITA /*&& !$ExisteCITA2*/){
										
					$array['user_id'] 	 = $ExisteVIN['user'];
					$array['vehicle_id'] = $ExisteVIN['vehicle_id'];						
								
					$interaccion_BPRO->adopta_vin($array['vin'], $value['BASE'], $array['user_id'], $array['vehicle_id']);								
					
					$interaccion_api->CrearCitaNube($array);
					
					$interaccion_api->actualiza_vehiculo($array['vehicle_id'], $array['marca'], $array['modelo'], $array['color_exterior'], $array['kilometraje']);
					$mesja = $value["CIT_NUMSERIE"]."Paso 2, Vin No esta adoptado";
					$interaccion_api->mensaje_usuario($array['user_id'], $array['data_schedule'], $array['hour_schedule'],$mesja);	
					
					$interaccion_BPRO->actualiza_app_citas($value['CAMBIO_ID']);
					$interaccion_api->actualiza_status_register_a_true($array['vehicle_id']);
				}
				else{

					//echo "<h1>".$value['CIT_NUMSERIE']."</h1>";
					//echo ($ExisteVIN == false)? "No existe VIN <br>" : "Existe VIN <br>";
					//echo (!$ExisteCITA)? "No existe cita <br>" : "Existe Cita <br>";					
				}
			}
			else{
					/*
					  1 - FECHA
				      2 - HORA
				      3 - ESTATUS
				      4 - KILOMETRAJE
				      5 - ASESOR
					*/
				//PREGUNTO SI EXISTE CITA CON MI FOLIO CDA
				$ExisteCITA = $interaccion_api->ConsultaCita($value['CIT_IDCITA']);		
		
				
				//SI EXISTE EL REGISTRO Y APARTE ES IGUAL A N NO SE HA REPLICADO EN LA APP
				if($ExisteCITA != false  && $value['ACTUALIZADO_EN_APP'] == "N"){
					
					
					$id_cita_nube 	  = $ExisteCITA[0]['schedule'];	
					$array['user_id'] = $value['ID_PROPIETARIO_NUBE'];			

					$interaccion_api->actualiza_cita($id_cita_nube,$value['CIT_IDCITA'],$value['CIT_HORCITA'],$value['CIT_FECCITA'],1);	
					//$mesja = "mensaje paso dos vin adoptado existe cita con CDA";
					$mesja = $value["CIT_NUMSERIE"]."Paso 2, Vin esta adoptado, existe cita con CDA";
					$interaccion_api->mensaje_usuario($array['user_id'], $array['data_schedule'], $array['hour_schedule'],$mesja);				      
				}
				elseif(!$ExisteCITA){//SI NO EXISTE LA CITA LA CREO
										
					$array['user_id'] 	 = $value['ID_PROPIETARIO_NUBE'];
					$array['vehicle_id'] = $value['ID_VEH_NUBE'];	

					$interaccion_api->CrearCitaNube($array);
					$interaccion_api->actualiza_vehiculo($array['vehicle_id'], $array['marca'], $array['modelo'], $array['color_exterior'], $array['kilometraje']);
					//$mesja = "mensaje paso dos vin adoptado y creo cita con CDAk"; 
					$mesja = $value["CIT_NUMSERIE"]."Paso 2, Vin esta adoptado, creo cita con CDA";
					$interaccion_api->mensaje_usuario($array['user_id'], $array['data_schedule'], $array['hour_schedule'],$mesja);	
					$interaccion_BPRO->actualiza_app_citas($value['CAMBIO_ID']);
					$interaccion_api->actualiza_status_register_a_true($array['vehicle_id']);
				}				
				else{ //SI NO EXISTE LA CITA CON EL FOLIO CDA, CONSULTO POR EL NUMERO DE SERIE
																				
					$arregloActualiza = array();

					$ExisteCITA2 = $interaccion_api->ConsultaCitaXVin($value['CIT_NUMSERIE']);

					$array['user_id'] 	 = $value['ID_PROPIETARIO_NUBE'];
					$array['vehicle_id'] = $value['ID_VEH_NUBE'];
					
					foreach ($ExisteCITA2 as $key => $Balor) {
						echo $Balor['schedule'];
						echo "<br>";
						$Uno = false;
						$Dos = false;
						
						if($Balor['vin'] == $value['CIT_NUMSERIE']){
							
							$Uno = true;

							if(!empty($Balor['sucursal']) && $Balor['sucursal']['sucursal'] == $value['ID_SUC_NUBE']){
								$arregloActualiza['sucursal'] = $Balor['sucursal']['sucursal'];
								$Dos = true;

							}
							elseif (!empty($Balor['bodyshops']) && $Balor['bodyshops']['bodyshops'] == $value['ID_BODY_NUBE']) {
								$arregloActualiza['bodyshops'] = $Balor['bodyshops']['bodyshops'];
								$Dos = true;
							}
						}

						if($Uno && $Dos){
							echo $Balor['sucursal']['sucursal'];
							echo "<br>";
							echo $Balor['vin'];
							echo "<br>";
							echo "<h1>HOLLLLA</h1>";
							$interaccion_api->actualiza_cita($Balor['schedule'],$value['CIT_IDCITA'],$value['CIT_HORCITA'],$value['CIT_FECCITA'],1);
							//$mesja = "mensaje paso dos vin adoptado y plancho cita nube y aplico folio CDA";
							$mesja = $value["CIT_NUMSERIE"]."Paso 2, Vin esta adoptado,plancho cita y coloco folio CDA";
							$interaccion_api->mensaje_usuario($array['user_id'], $array['data_schedule'], $array['hour_schedule'],$mesja);		
						}											
					}
					/*echo "<pre>";
					print_r($ExisteCITA2);					
					echo $value['ID_SUC_NUBE'];
					echo "</pre><br>";*/
					echo $value['CIT_NUMSERIE'];
					//echo "<h1>HOLLLLA</h1>";
				}
			}
		}

		echo "</table>";

		//ProcesaOrdenesSC();

		/*$resultados_citas_CDAP_VIN_NOTNULL = $conex_CDAP->ejecutarQuerySQL($sql_CITAS_CDAP_VIN_NOT_NULL);
		echo "<pre>";

		print_r($resultados_citas_CDAP_VIN_NOTNULL);
		echo "</pre>";*/
		//FIN DE CONSULTAS -------------------------------------------------------------------------------------------


	function total_paginas( $totalrows, $rowspagina ){
		return ceil($totalrows / $rowspagina);
	}


	/*IMPfunction ConsultaTierraCitas(){

		$conex_CDAP_v = new conServer
		_mssql('', "CDAP");

		$sql_CITAS_CDAP_WITHOUT_NULL = "SELECT appcitas.CAMBIO_ID ,appcitas.CIT_IDCITA ,
										appcitas.CIT_IDORDEN ,appcitas.CIT_NUMSERIE ,
										appcitas.CIT_FECCITA ,appcitas.CIT_HORCITA ,
										appcitas.CIT_IDSTATUSCIT ,appcitas.CIT_IDASESOR ,
										appcitas.BASE ,appcitas.INSERTADO_EN ,appcitas.OPERACION ,
										visCIT.ASESOR ,visCIT.MARCA ,visCIT.CIT_ANMODELO ,
										visCIT.cit_kilometraje KILOMETRAJE ,visCIT.CODIGO_EXTERIOR COLOR_EXT ,
										appcitas.REVISADO_SI_NO ,appcitas.REVISADO_ULTIMAVEZ ,
										appcitas.ACTUALIZADO_EN_APP ,appcitas.FECHA_ACTUALIZADO_APP ,
										app_VINS_ADOPTADOS.VIN VIN_ADOPTADO ,app_SUC_TIERRA_NUBE.ID_SUC_NUBE ,
										app_SUC_TIERRA_NUBE.ID_BODY_NUBE 
										FROM Bpro_ConectaBase.dbo.app_CITAS_CAMBIOS appcitas 
										LEFT JOIN Bpro_ConectaBase.dbo.visSER_CITASASESORES visCIT ON appcitas.CIT_IDCITA = visCIT.CIT_IDCITA 
										AND BASE = visCIT.NOMBRE_BASE 
										LEFT JOIN app_VINS_ADOPTADOS ON appcitas.CIT_NUMSERIE = app_VINS_ADOPTADOS.VIN 
										LEFT JOIN app_SUC_TIERRA_NUBE ON appcitas.BASE = app_SUC_TIERRA_NUBE.BASE 
										WHERE 1=1 
										AND appcitas.CIT_NUMSERIE != '' 
										AND appcitas.OPERACION = 'INS' 
										AND(appcitas.CIT_IDSTATUSCIT = 'ING') 
										AND CONVERT(DATE,appcitas.INSERTADO_EN,103) >= CONVERT(DATE,GETDATE())  
										--AND app_VINS_ADOPTADOS.VIN IS NOT NULL 
										ORDER BY CAMBIO_ID DESC";
		$result = $conex_CDAP_v->ejecutarQuerySQL($sql_CITAS_CDAP_WITHOUT_NULL);

		return $result;
	}*/

	// Funcion para obtener datos de la nube con base en los parametros de "status_register" y "pagina"
	/*IMP function consulta_vins_con_cita_en_1($pagina, $registrados_en_CDA, $folio_CDA = false){
	
		$curl = curl_init();	

		$folio_cda = ($folio_CDA !== false) ? "&folio_CDA=0" : '';
		
		$url = "http://3.85.25.112/api/v1/schedule/?status=1&status_register=".$registrados_en_CDA."&page=".$pagina.$folio_cda;
		
		curl_setopt_array($curl, array(
	  		CURLOPT_URL => $url,
	  		CURLOPT_RETURNTRANSFER => true,
	  		CURLOPT_ENCODING => "",
	  		CURLOPT_MAXREDIRS => 10,
	  		CURLOPT_TIMEOUT => 0,
	  		CURLOPT_FOLLOWLOCATION => true,
	  		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  		CURLOPT_CUSTOMREQUEST => "GET",
	  		CURLOPT_HTTPHEADER => array(
    				"Authorization: JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo0NCwidXNlcm5hbWUiOiJqZXN1cy5wYXpvc0BjZGFwZW5pbnN1bGEuY29tLm14IiwiZXhwIjoxNjEyNjI5NjYyLCJlbWFpbCI6Implc3VzLnBhem9zQGNkYXBlbmluc3VsYS5jb20ubXgifQ.xSA3Nyetvx1kFDHLuOQA1QeAoW_Pl_C0OXwi5XpoczE"
  			),
		));

		$response = curl_exec($curl);		
		
		curl_close($curl);		
		
		if ($err) {
			return "cURL Error #:" . $err;
		} 
		else {
			return $response;
		}		
	}*/

	function consulta_vins_registrados_cancelado($pagina, $registrados_en_CDA ){
	
	// {{url}}vehicle/?status_register=False&page=2  ( pagina 1, pagina 2 etc )
	
		$curl = curl_init();
		
		curl_setopt_array($curl, array(
	  		// CURLOPT_URL => "http://3.85.25.112/api/v1/vehicle/?status_register=True",
	  		CURLOPT_URL => "http://3.85.25.112/api/v1/vehicle/?status_register=".$registrados_en_CDA."&page=".$pagina,
	  		CURLOPT_RETURNTRANSFER => true,
	  		CURLOPT_ENCODING => "",
	  		CURLOPT_MAXREDIRS => 10,
	  		CURLOPT_TIMEOUT => 30,
	  		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  		CURLOPT_CUSTOMREQUEST => "GET",
	  		CURLOPT_HTTPHEADER => array(
				"Accept: */*",
				"Accept-Encoding: gzip, deflate",
				"Authorization: JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6ImdvbnphbG8iLCJleHAiOjE2MDUwNDM0ODIsImVtYWlsIjoiZ29uemFsby5ydWl6QGRhY29kZXMuY29tLm14In0.XMqroKYOPqSmHJ25vvPySdXDwnqBFnFqsGlF-KjjQDg",
				"Cache-Control: no-cache",
				"Connection: keep-alive",
				"Host: 3.85.25.112",
				"Postman-Token: 60868ee0-0140-4cf9-9bc8-fa3e8e850ecb,0dad5302-f269-4dd8-afd4-6659f48185f8",
				"User-Agent: PostmanRuntime/7.20.1",
				"cache-control: no-cache"
	  		),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if($err) {
			return "cURL Error #:" . $err;
		} 
		else {
			return $response;
		}
	}	

	/*IMP function ConsultaVIN($VIN){

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://3.85.25.112/api/v1/verifyvin",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => array('vin' => $VIN),
		  CURLOPT_HTTPHEADER => array(
		    "Authorization: JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo0NCwidXNlcm5hbWUiOiJqZXN1cy5wYXpvc0BjZGFwZW5pbnN1bGEuY29tLm14IiwiZXhwIjoxNjEyNjI5NjYyLCJlbWFpbCI6Implc3VzLnBhem9zQGNkYXBlbmluc3VsYS5jb20ubXgifQ.xSA3Nyetvx1kFDHLuOQA1QeAoW_Pl_C0OXwi5XpoczE",
		    "Content-Type: multipart/form-data; boundary=--------------------------203896728744017145457148"
		  ),
		));

		$response = curl_exec($curl);

		$response = json_decode($response, true);
		curl_close($curl);

		$resultado = $response;
		
		if(!$response['register']){
			$resultado = false;
		}
		
		//return $response['register'];
		return $resultado;
	}*/

	/*IMP function ConsultaCita($folioCDA){

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://3.85.25.112/api/v1/schedule/?folio_CDA=".$folioCDA,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "Authorization: JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo0NCwidXNlcm5hbWUiOiJqZXN1cy5wYXpvc0BjZGFwZW5pbnN1bGEuY29tLm14IiwiZXhwIjoxNjEyNjI5NjYyLCJlbWFpbCI6Implc3VzLnBhem9zQGNkYXBlbmluc3VsYS5jb20ubXgifQ.xSA3Nyetvx1kFDHLuOQA1QeAoW_Pl_C0OXwi5XpoczE"
		  ),
		));

		$response = curl_exec($curl);
		$response = json_decode($response, true);
		curl_close($curl);
		//print_r($response['data']);
		$respuesta = true;
		
		if(empty($response['data'])){
			$respuesta = false;
		}
		
		//return $response['data'];
		return $respuesta;
	}*/

//*****************************************************************************************************************************************************
	echo "<br>
		  <br>
		  <br>";
	
	$registrados_en_CDA 	= True;
	$pagina 				= 1;
	$folio_CDA 				= 0;

	$respuesta 			= $interaccion_api->consulta_vins_con_cita_en_1($pagina,$registrados_en_CDA,$folio_CDA);
	$vins_con_cita_en_ 	= json_decode($respuesta,true);

	echo "Filas totales:    ".$vins_con_cita_en_1["pagination"]["total_rows"]."<br>";
	echo "Filas por página: ".$vins_con_cita_en_1["pagination"]["per_page"]."<br>";
	
	$total_paginas 		= total_paginas( $vins_con_cita_en_1["pagination"]["total_rows"], $vins_con_cita_en_1["pagination"]["per_page"] );