<?php
	session_start();
	header('Content-Type: text/html; charset=utf-8');

	require_once("funciones_API.php"); 	
	require_once("funciones_consultasBPRO.php");	
	$interaccion_api  = new funciones_API();
	$interaccion_BPRO = new funciones_consultasBPRO();	
	
?>

<!DOCTYPE html>
<link href="tabla4a.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="jquery-1.8.2.min.js"></script>
<title>Actualiza APP CDA Órdenes</title>
<h2>Actualiza APP CDA Órdenes</h2>


<?php

	set_time_limit(0);

//---------------------------  BLOQUE DE CODIGO DONDE CONSULTAMOS LAS CITAS DE TIERRA ----------------------------------------------------------------------------

		$hora_inicio = date("Y-m-d H:i:s");
		echo "<p style='font-weight:bold ;color:red'> Inicio: ".$hora_inicio."</p>";

		$resultados_ordenes_CDAP = $interaccion_BPRO->ConsultaTierraOrdenes( 450 );
		
		// print_r($resultados_ordenes_CDAP);
		// exit();
		
		echo "<br>";						
		$k= 0;

		echo "<table border=1>
					<th>#</th>
					<th>cambio_id</th>
					<th>Id<br>cit id cita</th>
					<th>Id<br>ore id cita</th>
					<th>Id<br>id orden</th>
					<th>Serie</th>
		      		<th>Fecha<br>orden</th>
		      		<th>Hora<br>orden</th>
		      		<th>Orden<br>status</th>
		      		<th>Id<br>Asesor</th>
			  		<th>Base</th>
			  		<th>Insertado<br>en</th>
			  		<th>Operación</th>
			  		<th>Asesor</th>
			  		<th>Marca</th>
			  		<th>Modelo</th>
			  		<th>Kilometraje</th>
			  		<th>Color<br>Ext</th>
			  		<th>Revisado<br>S/N</th>
			  		<th>Última<br>revisión</th>
			  		<th>Actualizado<br>en APP</th>
			  		<th>Fecha<br>actualizado<br>en APP</th>
			  		<th>VIN_ADOPTADO</th>
			  		<th>SUC_NUBE</th>
			  		<th>BODY_NUBE</th>";
			  		
			if($resultados_ordenes_CDAP!=null)
			{
				$arregloConCitaCDA = array();		
				foreach($resultados_ordenes_CDAP as $fila_ordenes_CDAP)
				{
						// ----- empieza probamos si existe el vin en la nube, esto solamente se hace si el vin no está adoptado. ---------------
									$existe_vin = "no";
									if($fila_ordenes_CDAP['VIN_ADOPTADO'] == '')  // solo si no está adoptado vamos a preguntar a nube
									{
										
											// antes de preguntar a la nube, preguntamos a la base porque si adoptamos al vuelo, y traemos el resultset
											// en memoria, el primer registro que se adopte estará bien, pero el segundo seguirá trayendo el valor de adoptado = "no"
											// y como ya estará dado de alta en la tabla de vins_adoptados,  se intentará insertar dando un error de primary key
											// evitaremos esto preguntando a la base si ya existe en adoptados
											
											$resultado_existe_vin_ya_adoptado = $interaccion_BPRO->existe_vin_adoptado( $fila_ordenes_CDAP['ore_numserie'] );
											if ($resultado_existe_vin_ya_adoptado!=null)  // ya está adoptado
											{
												$ExisteVIN = false;
												$existe_vin = "si";
											}
											else   // no está adoptado, hay que preguntar en nube
											{
												// empieza preguntar a la nube si existe el vin
											
												// si no está adoptado,  preguntar a la nube si el vin existe registrado
												$ExisteVIN  = $interaccion_api->ConsultaVIN($fila_ordenes_CDAP['ore_numserie']);
												// $ExisteVIN  = ConsultaVIN($fila_ordenes_CDAP['ore_numserie']);
													   // este endpoint devuelve 3 cosas, y 2 nos interesan  para enviarlas a adoptar vin:
													   // $ExisteVIN['user']  y  $ExisteVIN['vehicle_id'];													
											}										
																		
											if($ExisteVIN != false)  // aqui existe el vin en nube
											{
												$fondo1 = "yellow";
												$letra1 = "black";
												// se adopta el vin !
												$interaccion_BPRO->adopta_vin($fila_ordenes_CDAP["ore_numserie"], $fila_ordenes_CDAP['base'], $ExisteVIN['user'], $ExisteVIN['vehicle_id'] );
												// adopta_vin( $fila_ordenes_CDAP["ore_numserie"], $fila_ordenes_CDAP['base'], $ExisteVIN['user'], $ExisteVIN['vehicle_id'] );
												$existe_vin = "si";
											}
											else
											{
												$fondo1 = "#f6f6f6"; // gris igual a todo
												$letra1 = "black";
											}							
										
									}
									else  // está adoptado
									{
										// $ExisteVIN  = $interaccion_api->ConsultaVIN($fila_ordenes_CDAP['ore_numserie']);
										$ExisteVIN['user']       = $fila_ordenes_CDAP['ID_PROPIETARIO_NUBE'];
										$ExisteVIN['vehicle_id'] = $fila_ordenes_CDAP['ID_VEH_NUBE'];
										
										$fondo1 = "#50D050"; // verde
										$letra1 = "white";
										$existe_vin = "si";
									}
						//  ----  termina el probar si existe el vin en nube, ya tenemos un si o un no ( existe o no existe, si existe y no lo teníamos fue adoptado )
						
						// ahora debemos checar si la cita existe, el razonamiento será:
						// si la cita no existe, y el vin si existe (  $existe_vin = "si" )  y si  vino SIN CITA ( ore_idcita = 0 ) entonces se crea la cita
						
						// si la cita, existe, se actualizan los datos de la cita
						/*
						 con el caso del vin 1FDWF3G6XHEB87472  que tenía 2 cit_idcita ( 65050 el 18-mar-2020 a las 8:00  y el 65064 el 18-mar-2020 a las 8:17 )
						 se vió que hay que poner un IF   que exprese lo siguiente:
						 
						  si  
							
						  ID  ORE_IDCITA ES DIFERENTE A CERO, TOMO ESO ( ORE_IDCITA ) COMO FOLIO DE CITA

						  ID  ORE_IDCITA ES CERO, TOMO ENTONCES  ID CIT_IDCITA COMO FOLIO DE CITA						 
						 
						*/
						
						if (  $fila_ordenes_CDAP["ore_idcita"] != 0   )
						{
							$folioCDA = $fila_ordenes_CDAP["ore_idcita"];
						}
						else
						{
							$folioCDA = $fila_ordenes_CDAP["cit_idcita"];
						}
						
						$existe_cita = $interaccion_api->ConsultaCita($folioCDA);						
						
						if( $existe_vin == "si" )
						{
							if( $fila_ordenes_CDAP["ore_idcita"] == 0 and $existe_cita == false  )     // si el vin ya existe porque fue adoptado o está adoptado,            
						    {                                                                          // y el cliente LLEGÓ SIN CITA, 
						                                                                               // PROCEDEMOS A CREAR LA CITA EN ESTATUS 2
									// CREAMOS CITA  EN ESTATUS 2, previo chequeo de que no exista y 
									$array['status_id']   = 2;
									// ojo, estos 2 no los tengo si ya está adoptado, vigilar esto:
									$array['user_id'] 		= $ExisteVIN['user'];
									$array['vehicle_id'] 	= $ExisteVIN['vehicle_id'];

									$array['vin'] 			= $fila_ordenes_CDAP['ore_numserie'];
									$array['hour_schedule'] = $fila_ordenes_CDAP['ore_horaord'];
									$array['data_schedule'] = $fila_ordenes_CDAP['ore_fechaord'];
									$array['sucursal'] 		= $fila_ordenes_CDAP['id_suc_nube'];
									$array['folio_CDA'] 	= $fila_ordenes_CDAP['cit_idcita'];

									//Mnesaje
									$array['asesor_cita']   = $fila_ordenes_CDAP['asesor'];

									//Actualiza vehiculo
									$array['marca']				= $fila_ordenes_CDAP['marca'];	
									$array['modelo']			= $fila_ordenes_CDAP['anmodelo'];	
									$array['color_exterior']	= $fila_ordenes_CDAP['COLOR_EXT'];	
									$array['kilometraje']		= $fila_ordenes_CDAP['ore_kilometraje'];					
									

									// $interaccion_api->adopta_vin( $key['VIN'], $key['Base_adopto'], $key['Id_propietario_nube'], $key['Id_vehiculo_nube']);			
									$interaccion_api->CrearCitaNube($array);
									$interaccion_api->actualiza_vehiculo($array['vehicle_id'], $array['marca'], $array['modelo'], $array['color_exterior'], $array['kilometraje']);
									
									// $mensaje_usuario = 'Su asesor es '.$array['asesor_cita'];
									$mensaje_usuario = $array['vin'].
									' - Orden estatus('.$array['status_id'].') '.$fila_ordenes_CDAP['ore_status'];
									$interaccion_api->mensaje_usuario($array['user_id'], $array['data_schedule'], $array['hour_schedule'], $mensaje_usuario );									
									$interaccion_BPRO->actualiza_app_ordenes($fila_ordenes_CDAP['cambio_id']);
							}
							
							if
							( $existe_cita != false   )    	// en todos los demás casos actualizaremos los datos de la cita
							{								// en este caso al ser NOT FALSE, es que trae un array con todos los datos de la cita
							                                // aquí se deberá actualizar los datos de la cita, es donde se actualizará el ESTATUS
															// SE DEBE DE PENSAR CÓMO SE MANEJARÁ ESTO DEL ESTATUS PARA HACER UNA TABLA DE EQUIVALENCIA DE ESTATUS
															// ENTRE BPRO Y NUBE
								/*
								echo "<pre>";
								print_r($existe_cita);
								echo "</pre>";
								echo "<hr>";
								echo "<h2>".$existe_cita[0]["schedule"]."</h2>";
								echo "<hr>";
								*/
								$id_cita_nube = $existe_cita[0]["schedule"];
								
								// solamente se actualizan 3 parámetros.... pero..... está mal... hay que tener la tabla de equivalencia
								// con una función porque el cambio de estatus es numérico y bpro es alfanumérico
								// esta tabla de equivalencia se generará en la función "equivalencia_estatus_ordenes()"
								
								// transforma de BPRO  a APP el estatus de la orden
								$estatus_orden = equivalencia_estatus_ordenes( $fila_ordenes_CDAP["ore_status"] );
								echo $estatus_orden."</br>";
								
								/*
								$interaccion_api->actualiza_cita( 	$id_cita_nube,  $fila_ordenes_CDAP['cit_idcita'],
																	$fila_ordenes_CDAP['ore_horaord'], 
																	$fila_ordenes_CDAP['ore_fechaord'], 
																	$estatus_orden 
																);
								*/
								
								$interaccion_api->actualiza_cita( 	$id_cita_nube,  $folioCDA,
																	$fila_ordenes_CDAP['ore_horaord'], 
																	$fila_ordenes_CDAP['ore_fechaord'], 
																	$estatus_orden 
																);								
																
								// $interaccion_api->actualiza_vehiculo($array['vehicle_id'], $array['marca'], $array['modelo'], $array['color_exterior'], $array['kilometraje']);
								
								// $mensaje_usuario = 'Su asesor es '.utf8_encode($fila_ordenes_CDAP["asesor"]);
								$mensaje_usuario = $fila_ordenes_CDAP["ore_numserie"].' - Orden estatus('.$estatus_orden.') '.$fila_ordenes_CDAP["ore_status"];
								
								$interaccion_api->mensaje_usuario(  $fila_ordenes_CDAP['ID_PROPIETARIO_NUBE'], 
																	$fila_ordenes_CDAP['ore_fechaord'], 
																	$fila_ordenes_CDAP['ore_horaord'],
                                                                    $mensaje_usuario );
								
								$interaccion_BPRO->actualiza_app_ordenes($fila_ordenes_CDAP['cambio_id']);
								
							}

						}
					
						$k++;
						echo "<tr>";
						echo "<td>".$k."</td>";
						echo "<td>".$fila_ordenes_CDAP["cambio_id"]."</td>";
						echo "<td>".$fila_ordenes_CDAP["cit_idcita"]."</td>";
						echo "<td>".$fila_ordenes_CDAP["ore_idcita"]."</td>";
						echo "<td>".$fila_ordenes_CDAP["ore_idorden"]."</td>";
						echo "<td style='font-weight: normal; background:".$fondo1."; color:".$letra1.";'>".$fila_ordenes_CDAP["ore_numserie"]." - ".$id_cita_nube."</td>";
						echo "<td>".$fila_ordenes_CDAP["ore_fechaord"]."</td>";
						echo "<td>".$fila_ordenes_CDAP["ore_horaord"]."</td>";
						echo "<td>".$fila_ordenes_CDAP["ore_status"]."</td>";
						echo "<td>".$fila_ordenes_CDAP["ore_idasesor"]."</td>";
						echo "<td>".$fila_ordenes_CDAP["base"]."</td>";
						echo "<td style='width:130px'>".$fila_ordenes_CDAP["insertado_en"]."</td>";
						echo "<td>".$fila_ordenes_CDAP["operacion"]."</td>";
						echo "<td>".utf8_encode($fila_ordenes_CDAP["asesor"])."</td>";
						echo "<td>".ucfirst(strtolower($fila_ordenes_CDAP["marca"]))."</td>";
						echo "<td>".$fila_ordenes_CDAP["anmodelo"]."</td>";
						echo "<td>".$fila_ordenes_CDAP["ore_kilometraje"]."</td>";
						echo "<td>".utf8_encode($fila_ordenes_CDAP["COLOR_EXT"])."</td>";
						echo "<td>".$fila_ordenes_CDAP["revisado_si_no"]."</td>";
						echo "<td>".$fila_ordenes_CDAP["revisado_ultimavez"]."</td>";
						echo "<td>".$fila_ordenes_CDAP["actualizado_en_app"]."</td>";
						echo "<td>".$fila_ordenes_CDAP["fecha_actualizado_app"]."</td>";
						echo "<td>".$fila_ordenes_CDAP["VIN_ADOPTADO"]."</td>";
						echo "<td>".$fila_ordenes_CDAP["id_suc_nube"]."</td>";
						echo "<td>".$fila_ordenes_CDAP["id_body_nube"]."</td>";					
						echo "</tr>";
				}

			}
			
		echo "</table>";


function equivalencia_estatus_ordenes( $estatus_bpro )
{

/*
--------------------------------------------------------------------------------------
      ESTATUS ORDENES EN BPRO          ESTATUS ORDENES APP ( EQUIVALENCIAS )
--------------------------------------------------------------------------------------
      A   ABIERTA                      2  ( APERTURA )
      C   CANCELADA                    - dejamos 2 - no hay cambio ( no contemplado )
      D   DETENIDA                     - dejamos 2 - no hay cambio ( no contemplado )
      I   FACTURADA                    5  ( ENTREGADO )
      P   PROCESO                      2  ( APERTURA )
      T   CERRADA                      3  ( CIERRE DE ORDEN )
--------------------------------------------------------------------------------------
                                       orden  "natural" :
                                       
                                       1   CITA
                                       2   APERTURA
                                       3   CIERRE ORDEN
                                       4   PAGAR
                                       5   ENTREGADO
--------------------------------------------------------------------------------------
*/

		switch ( $estatus_bpro )
		{
			case "A":
				$estatus_app = 2;
				break;
			case "C":
				$estatus_app = 2;
				break;
			case "D":
				$estatus_app = 2;
				break;
			case "I":
				$estatus_app = 5;
				break;		
			case "P":
				$estatus_app = 2;
				break;		
			case "T":
				$estatus_app = 3;
				break;		
		}

	return $estatus_app;
}

?>