<?php		
	include("conServer_mssql.php");
	
	class funciones_consultasBPRO{
			
		
		//var $conex_CDAP = new conServer_mssql('', "CDAP");

		public function GetDatosCita_BPRO($cadena_vins_no_reg){

			$conex_CDAP = new conServer_mssql('', "CDAP");

			$sql_CITAS_CDAP = "SELECT 
						  	appcitas.CAMBIO_ID
							,appcitas.CIT_IDCITA
							,appcitas.CIT_IDORDEN
							,appcitas.CIT_NUMSERIE
							,appcitas.CIT_FECCITA
							,appcitas.CIT_HORCITA
							,appcitas.CIT_IDSTATUSCIT
							,appcitas.CIT_IDASESOR
					 		,appcitas.BASE
					 		,appcitas.INSERTADO_EN
					 		,appcitas.OPERACION
							,visCIT.ASESOR
							,visCIT.MARCA
							,visCIT.CIT_ANMODELO
							,visCIT.cit_kilometraje KILOMETRAJE
							,visCIT.CODIGO_EXTERIOR COLOR_EXT
					 		,appcitas.REVISADO_SI_NO 
					 		,appcitas.REVISADO_ULTIMAVEZ
					 		,appcitas.ACTUALIZADO_EN_APP
					 		,appcitas.FECHA_ACTUALIZADO_APP
					 		,app_VINS_ADOPTADOS.VIN VIN_ADOPTADO
					 		,app_SUC_TIERRA_NUBE.ID_SUC_NUBE
					 		,app_SUC_TIERRA_NUBE.ID_BODY_NUBE
							FROM Bpro_ConectaBase.dbo.app_CITAS_CAMBIOS appcitas
					  		LEFT JOIN Bpro_ConectaBase.dbo.visSER_CITASASESORES visCIT ON appcitas.CIT_IDCITA = visCIT.CIT_IDCITA AND BASE = visCIT.NOMBRE_BASE
					  		LEFT JOIN app_VINS_ADOPTADOS ON appcitas.CIT_NUMSERIE = app_VINS_ADOPTADOS.VIN
					  		LEFT JOIN app_SUC_TIERRA_NUBE ON appcitas.BASE = app_SUC_TIERRA_NUBE.BASE
							WHERE 1=1
							AND appcitas.CIT_NUMSERIE != ''
							AND appcitas.OPERACION = 'INS' AND ( appcitas.CIT_IDSTATUSCIT = 'ING' ) -- OR CIT_IDSTATUSCIT = 'NRE' )
							AND CONVERT(DATE,appcitas.CIT_FECCITA,103) >= CONVERT(DATE,GETDATE())
								-- AND SUBSTRING( CIT_IDORDEN, 1,1 ) = 'G'
							AND appcitas.CIT_NUMSERIE
							IN (".$cadena_vins_no_reg.")
							AND app_VINS_ADOPTADOS.VIN IS NULL
							ORDER BY CAMBIO_ID ASC  
							-- el cambio más viejo se refleja antes que el cambio más nuevo, por eso es ASC
							-- CONVERT(DATETIME,appcitas.CIT_FECCITA+' '+appcitas.CIT_HORCITA,103) DESC
							";
		
			$resultados_citas_CDAP = $conex_CDAP->ejecutarQuerySQL($sql_CITAS_CDAP);

			return $resultados_citas_CDAP;
		}
		
		public function ConsultaTierraOrdenes( $registros ){

			$conex_CDAP = new conServer_mssql('', "CDAP");

			$sql_ORDENES_CDAP = "select top $registros
								   appordenes.cambio_id
										,appordenes.cit_idcita
								  ,appordenes.ore_idcita
								  ,appordenes.ore_idorden
								  ,appordenes.ore_numserie
								  ,appordenes.ore_fechaord
								  ,appordenes.ore_horaord
								  ,appordenes.ore_status
								  ,appordenes.ore_idasesor

								  ,appordenes.base
								  ,appordenes.insertado_en
								  ,appordenes.operacion
										,visORDEN.asesor
										,visORDEN.ORE_MARCA marca
										,visORDEN.ANMODELO+'  '+visORDEN.TIPOAUTO [anmodelo]
										,visORDEN.COLOEXTE [COLOR_EXT] 
								        ,appordenes.ore_kilometraje										
									   -- visCIT.cit_kilometraje KILOMETRAJE, <--- este no va, ya está en ordenes_cambios ( arriba ore_kilometraje )
								  ,appordenes.revisado_si_no
								  ,appordenes.revisado_ultimavez
								  ,appordenes.actualizado_en_app
								  ,appordenes.fecha_actualizado_app
									  ,app_vins_adoptados.vin VIN_ADOPTADO
									  ,app_suc_tierra_nube.id_suc_nube
									  ,app_suc_tierra_nube.id_body_nube
									  ,app_vins_adoptados.ID_VEH_NUBE
									  ,app_vins_adoptados.ID_PROPIETARIO_NUBE
							  from bpro_conectabase.dbo.app_ordenes_cambios appordenes
										LEFT JOIN Bpro_ConectaBase.dbo.visSER_ORDENVIN visORDEN ON appordenes.ORE_IDORDEN = visORDEN.ORE_IDORDEN AND BASE = visORDEN.NOMBRE_BASE	
								   LEFT JOIN app_vins_adoptados 
										  ON appordenes.ore_numserie = app_vins_adoptados.vin 
								   LEFT JOIN app_suc_tierra_nube 
										  ON appordenes.base = app_suc_tierra_nube.base   
							WHERE  1 = 1 
								   AND appordenes.operacion = 'INS' 
								   AND CONVERT(DATE, appordenes.insertado_en, 103) >= CONVERT(DATE, Getdate())
								   AND SUBSTRING(appordenes.ORE_IDORDEN,1,1) = 'N'
								   AND REVISADO_SI_NO = 'N'
								   -- and appordenes.BASE <> 'CDAP_YBody'
								   -- AND CONVERT(DATE, appordenes.ORE_FECHAORD, 103) >= CONVERT(DATE, Getdate()) 
								   -- AND appordenes.ORE_IDORDEN = 'N02046491'
								   -- AND CIT_IDCITA is null
							ORDER  BY cambio_id asc
							-- ORDER  BY cambio_id desc
							";
		
			$resultados_ordenes_CDAP = $conex_CDAP->ejecutarQuerySQL($sql_ORDENES_CDAP);	

			return $resultados_ordenes_CDAP;
		}		

		public function ConsultaTierraCitas(){

			$conex_CDAP = new conServer_mssql('', "CDAP");

			$sql_CITAS_CDAP_WITHOUT_NULL = "SELECT appcitas.CAMBIO_ID ,appcitas.CIT_IDCITA ,
											appcitas.CIT_IDORDEN ,appcitas.CIT_NUMSERIE ,
											appcitas.CIT_FECCITA ,appcitas.CIT_HORCITA ,
											appcitas.CIT_IDSTATUSCIT ,appcitas.CIT_IDASESOR ,
											appcitas.BASE ,appcitas.INSERTADO_EN ,appcitas.OPERACION ,
											visCIT.ASESOR ,visCIT.MARCA ,visCIT.CIT_ANMODELO ,
											visCIT.cit_kilometraje KILOMETRAJE ,visCIT.CODIGO_EXTERIOR COLOR_EXT ,
											appcitas.REVISADO_SI_NO ,appcitas.REVISADO_ULTIMAVEZ ,
											appcitas.ACTUALIZADO_EN_APP ,appcitas.FECHA_ACTUALIZADO_APP ,
											app_VINS_ADOPTADOS.VIN VIN_ADOPTADO, app_vins_adoptados.ID_VEH_NUBE
									  		, app_vins_adoptados.ID_PROPIETARIO_NUBE, app_SUC_TIERRA_NUBE.ID_SUC_NUBE ,
											app_SUC_TIERRA_NUBE.ID_BODY_NUBE 
											FROM Bpro_ConectaBase.dbo.app_CITAS_CAMBIOS appcitas 
											LEFT JOIN Bpro_ConectaBase.dbo.visSER_CITASASESORES visCIT ON appcitas.CIT_IDCITA = visCIT.CIT_IDCITA 
											AND BASE = visCIT.NOMBRE_BASE 
											LEFT JOIN app_VINS_ADOPTADOS ON appcitas.CIT_NUMSERIE = app_VINS_ADOPTADOS.VIN 
											LEFT JOIN app_SUC_TIERRA_NUBE ON appcitas.BASE = app_SUC_TIERRA_NUBE.BASE 
											WHERE 1=1 
											AND appcitas.CIT_NUMSERIE != '' 
											AND appcitas.OPERACION = 'INS' 
											AND(appcitas.CIT_IDSTATUSCIT = 'ING') 
											AND CONVERT(DATE,appcitas.INSERTADO_EN,103) >= CONVERT(DATE,GETDATE())  
											--AND app_VINS_ADOPTADOS.VIN IS NOT NULL 
											ORDER BY CAMBIO_ID DESC";
			
			$result = $conex_CDAP->ejecutarQuerySQL($sql_CITAS_CDAP_WITHOUT_NULL);

			return $result;
		}
		

		public function existe_vin_adoptado( $vin ){

			$conex_CDAP = new conServer_mssql('', "CDAP");

			$sql_existe_vin_adoptado = "SELECT
											  VIN
											 ,BASE
											 ,FECHA_HORA_ADOPCION
											 ,ID_VEH_NUBE
											 ,ID_PROPIETARIO_NUBE
											FROM dbo.app_VINS_ADOPTADOS
											WHERE VIN = '$vin'";
			
			$result = $conex_CDAP->ejecutarQuerySQL($sql_existe_vin_adoptado);

			return $result;
		}	
		

		public function adopta_vin( $vin, $base, $id_propietario_nube, $id_vehiculo_nube ){

			// include("conServer_mssql.php"); <-- si se deja da error por doble declaración

			// el primer parámetro va vacío, el primer parámetro es base de datos, si va vacío la base por default es bpro_conectabase
			// el segundo parámetro implica con qué empresa tratamos, o sea, a cuál conjunto de bases de qué servidor nos estamos conectando ( 5.5 o 5.22 )
			$conex_CDAP = new conServer_mssql('', "CDAP");

			$consulta_inserta = "INSERT app_VINS_ADOPTADOS ( VIN, BASE, FECHA_HORA_ADOPCION,  ID_VEH_NUBE,      ID_PROPIETARIO_NUBE )
													VALUES ( '".$vin."', '".$base."', GETDATE(),            ".$id_vehiculo_nube.", ".$id_propietario_nube." );";

			$estatus_modifica = $conex_CDAP->ejecutarComandoSQL($consulta_inserta);
			if ($estatus_modifica !="1")
			{
				echo "<h2>ERROR conexión SQL, operación abortada</h2>"; 
			}
			else
			{
				// si no hay problema en el sql y la inserción se llevó a cabo, modifico la nube
				 //actualiza_status_register_a_true( $id_vehiculo_nube );
			}
			return $estatus_modifica;
		} 

		public function actualiza_app_citas($idCambio){

			$conex_CDAP = new conServer_mssql('', "CDAP");
			/*
			update Bpro_ConectaBase.dbo.app_CITAS_CAMBIOS 
			SET REVISADO_SI_NO='S', REVISADO_ULTIMAVEZ=GETDATE(), ACTUALIZADO_EN_APP='S', FECHA_ACTUALIZADO_APP=GETDATE()
			WHERE CAMBIO_ID = 2910;
			*/
			$consulta_actualiza = "UPDATE Bpro_ConectaBase.dbo.app_CITAS_CAMBIOS
								 SET REVISADO_SI_NO = 'S', REVISADO_ULTIMAVEZ = GETDATE(), ACTUALIZADO_EN_APP='S',FECHA_ACTUALIZADO_APP=GETDATE()
								 WHERE CAMBIO_ID = ".$idCambio.";";
								
								//;";//VALUES ( '".$vin."', '".$base."', GETDATE(),            ".$id_vehiculo_nube.", ".$id_propietario_nube." );";

			$estatus_modifica = $conex_CDAP->ejecutarComandoSQL($consulta_actualiza);
			if ($estatus_modifica !="1")
			{
				echo "<h2>ERROR conexión SQL, operación abortada</h2>";
			}
			else
			{
				// si no hay problema en el sql y la inserción se llevó a cabo, modifico la nube
				// actualiza_status_register_a_true( $id_vehiculo_nube );
			}
		}
		
		
		public function bitacorea_app_mensajes($titulo,$mensaje,$id_propietario){
			
			/*
				jm -> 20-mar-2020  se puso para que la consulta de si llegó el mensaje en los teléfonos sea "un apoyo", pero tengamos una constancia
									de cada mensaje que se envió 
			*/
			
			$conex_CDAP = new conServer_mssql('', "CDAP");
			
			$consulta_actualiza = "INSERT app_MENSAJES
									 ( MENS_TITULO, MENS_MENSAJE, MENS_ID_PROPIETARIO, INSERTADO_EN )
									 VALUES
									 ( '$titulo', '$mensaje', $id_propietario, GETDATE() );";
								

			$estatus_modifica = $conex_CDAP->ejecutarComandoSQL($consulta_actualiza);
			if ($estatus_modifica !="1")
			{
				echo "<h2>ERROR conexión SQL, operación abortada</h2>";
			}
			else
			{
				// si no hay problema en el sql y la inserción se llevó a cabo, modifico la nube
				// actualiza_status_register_a_true( $id_vehiculo_nube );
			}
		}		
		

		public function actualiza_app_ordenes($idCambio){

			$conex_CDAP = new conServer_mssql('', "CDAP");
			/*
			update Bpro_ConectaBase.dbo.app_CITAS_CAMBIOS 
			SET REVISADO_SI_NO='S', REVISADO_ULTIMAVEZ=GETDATE(), ACTUALIZADO_EN_APP='S', FECHA_ACTUALIZADO_APP=GETDATE()
			WHERE CAMBIO_ID = 2910;
			*/
			$consulta_actualiza = "UPDATE Bpro_ConectaBase.dbo.app_ORDENES_CAMBIOS
								 SET REVISADO_SI_NO = 'S', REVISADO_ULTIMAVEZ = GETDATE(), ACTUALIZADO_EN_APP='S',FECHA_ACTUALIZADO_APP=GETDATE()
								 WHERE CAMBIO_ID = ".$idCambio.";";
								
								//;";//VALUES ( '".$vin."', '".$base."', GETDATE(),            ".$id_vehiculo_nube.", ".$id_propietario_nube." );";

			$estatus_modifica = $conex_CDAP->ejecutarComandoSQL($consulta_actualiza);
			if ($estatus_modifica !="1")
			{
				echo "<h2>ERROR conexión SQL, operación abortada</h2>";
			}
			else
			{
				// si no hay problema en el sql y la inserción se llevó a cabo, modifico la nube
				// actualiza_status_register_a_true( $id_vehiculo_nube );
			}
		}		
		
		
		
	}
?>