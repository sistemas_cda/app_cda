<?php

	require_once("funciones_consultasBPRO.php");	

	class funciones_API{

		public function consulta_vins_con_cita_en_1($pagina, $registrados_en_CDA, $folio_CDA = false){
	
			$curl = curl_init();	

			$folio_cda = ($folio_CDA !== false) ? "&folio_CDA=0" : '';
		
			$url = "http://3.85.25.112/api/v1/schedule/?status=1&status_register=".$registrados_en_CDA."&page=".$pagina.$folio_cda;
		
			curl_setopt_array($curl, array(
		  		CURLOPT_URL => $url,
		  		CURLOPT_RETURNTRANSFER => true,
		  		CURLOPT_ENCODING => "",
		  		CURLOPT_MAXREDIRS => 10,
		  		CURLOPT_TIMEOUT => 0,
		  		CURLOPT_FOLLOWLOCATION => true,
		  		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  		CURLOPT_CUSTOMREQUEST => "GET",
		  		CURLOPT_HTTPHEADER => array(
	    				"Authorization: JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo0NCwidXNlcm5hbWUiOiJqZXN1cy5wYXpvc0BjZGFwZW5pbnN1bGEuY29tLm14IiwiZXhwIjoxNjEyNjI5NjYyLCJlbWFpbCI6Implc3VzLnBhem9zQGNkYXBlbmluc3VsYS5jb20ubXgifQ.xSA3Nyetvx1kFDHLuOQA1QeAoW_Pl_C0OXwi5XpoczE"
	  			),
			));

			$response = curl_exec($curl);		
		
			curl_close($curl);		
		
			if($err){
				return "cURL Error #:" . $err;
			} 
			else{
				return $response;
			}		
		}

		public function ConsultaVIN($VIN){

			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => "http://3.85.25.112/api/v1/verifyvin",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => array('vin' => $VIN),
			  CURLOPT_HTTPHEADER => array(
			    "Authorization: JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo0NCwidXNlcm5hbWUiOiJqZXN1cy5wYXpvc0BjZGFwZW5pbnN1bGEuY29tLm14IiwiZXhwIjoxNjEyNjI5NjYyLCJlbWFpbCI6Implc3VzLnBhem9zQGNkYXBlbmluc3VsYS5jb20ubXgifQ.xSA3Nyetvx1kFDHLuOQA1QeAoW_Pl_C0OXwi5XpoczE",
			    "Content-Type: multipart/form-data; boundary=--------------------------203896728744017145457148"
			  ),
			));

			$response = curl_exec($curl);

			$response = json_decode($response, true);
			curl_close($curl);
			print_r($response);
			$resultado = $response;
			
			if(!$response['register']){
				$resultado = false;
			}			
			return $resultado;
		}

		public function ConsultaCita($folioCDA){

			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => "http://3.85.25.112/api/v1/schedule/?folio_CDA=".$folioCDA,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_HTTPHEADER => array(
			    "Authorization: JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo0NCwidXNlcm5hbWUiOiJqZXN1cy5wYXpvc0BjZGFwZW5pbnN1bGEuY29tLm14IiwiZXhwIjoxNjEyNjI5NjYyLCJlbWFpbCI6Implc3VzLnBhem9zQGNkYXBlbmluc3VsYS5jb20ubXgifQ.xSA3Nyetvx1kFDHLuOQA1QeAoW_Pl_C0OXwi5XpoczE"
			  ),
			));

			$response = curl_exec($curl);
			$response = json_decode($response, true);
			curl_close($curl);
			//print_r($response['data']);
			$respuesta = $response['data'];
			//print_r($respuesta);
			if(empty($response['data'])){
				$respuesta = false;
			}
			
			//return $response['data'];
			return $respuesta;
		}

		public function actualiza_vehiculo( $id_vehiculo_nube, $marca, $modelo, $color_exterior, $kilometraje){

			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => "http://3.85.25.112/api/v1/vehicle/".$id_vehiculo_nube."/",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "PATCH",
			  CURLOPT_POSTFIELDS => array('model' => ''.$modelo.'','mileage' => ''.$kilometraje.'','color' => ''.$color_exterior.'','brand' => ''.ucfirst($marca).''),
			  CURLOPT_HTTPHEADER => array(
				"Authorization: JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6ImdvbnphbG8iLCJleHAiOjE2MDUwNDM0ODIsImVtYWlsIjoiZ29uemFsby5ydWl6QGRhY29kZXMuY29tLm14In0.XMqroKYOPqSmHJ25vvPySdXDwnqBFnFqsGlF-KjjQDg"
			  ),
			));

			$response = curl_exec($curl);

			curl_close($curl);

				if ($err){
					return "cURL Error #:" . $err;
				} 
				else{
					return $response;
				}				
		}

		public function actualiza_status_register_a_true($id_vehiculo_nube){
	
			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => "http://3.85.25.112/api/v1/vehicle/".$id_vehiculo_nube."/",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "PATCH",
			  CURLOPT_POSTFIELDS => array('status_register' => 'True'),
			  CURLOPT_HTTPHEADER => array(
				"Authorization: JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6ImdvbnphbG8iLCJleHAiOjE2MDUwNDM0ODIsImVtYWlsIjoiZ29uemFsby5ydWl6QGRhY29kZXMuY29tLm14In0.XMqroKYOPqSmHJ25vvPySdXDwnqBFnFqsGlF-KjjQDg",
				"Content-Type: multipart/form-data; boundary=--------------------------890551227456016997244957"
			  ),
			));

			$response = curl_exec($curl);

			curl_close($curl);

				if ($err) 
				{
					return "cURL Error #:" . $err;
				} 
				else 
				{
					return $response;
				}
		}

		public function adopta_cita($id_cita,$id_cita_tierra, $hora_cita_tierra, $Fecha_cita_tierra){//$id_vehiculo_nube, $marca, $modelo, $color_exterior, $kilometraje ){
		
			$curl = curl_init();

			$FechaDividida = explode('/', $Fecha_cita_tierra);

			$dia = $FechaDividida[0];
			$mes = $FechaDividida[1];
			$anio = $FechaDividida[2];

			$fechaCorrecta = $anio."-".$mes."-".$dia;
			//echo $format = date("Y/m/d", strtotime($Fecha_cita_tierra))."<br>";

			$datos = array(
				"hour_schedule" => $hora_cita_tierra,
				"data_schedule" => $fechaCorrecta,//$Fecha_cita_tierra,
				"folio_CDA" => $id_cita_tierra
			);

			curl_setopt_array($curl, array(
		  		CURLOPT_URL => "http://3.85.25.112/api/v1/schedule/".$id_cita."/",
		  		CURLOPT_RETURNTRANSFER => true,
		  		CURLOPT_ENCODING => "",
		  		CURLOPT_MAXREDIRS => 10,
		  		CURLOPT_TIMEOUT => 0,
		  		CURLOPT_FOLLOWLOCATION => true,
		  		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  		CURLOPT_CUSTOMREQUEST => "PATCH",
		  		CURLOPT_POSTFIELDS => $datos,
		  		CURLOPT_HTTPHEADER => array(
	    								"Authorization: JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6ImdvbnphbG8iLCJleHAiOjE2MDUwNDM0ODIsImVtYWlsIjoiZ29uemFsby5ydWl6QGRhY29kZXMuY29tLm14In0.XMqroKYOPqSmHJ25vvPySdXDwnqBFnFqsGlF-KjjQDg",
	    								"Content-Type: multipart/form-data; boundary=--------------------------085622720903280288307670"
	    							)
				)
			);

			$response = curl_exec($curl);		

			curl_close($curl);

			if($err){
				return "cURL Error #:" . $err;
			} 
			else{
				return $response;
			}
		}

		public function CrearCitaNube($arreglo){
		
			$curl = curl_init();
			echo "crea cita";
			print_r($arreglo);
			echo "fin datos";

			$FechaDividida = explode('/', $arreglo['data_schedule']);

			$dia = $FechaDividida[0];
			$mes = $FechaDividida[1];
			$anio = $FechaDividida[2];

			$fechaCorrecta = $anio."-".$mes."-".$dia;
			$arreglo['data_schedule'] = $fechaCorrecta;

			$datos = array(

				'user_id' => $arreglo['user_id'],
				'vin' => $arreglo['vin'],
				'hour_schedule' => $arreglo['hour_schedule'],
				'data_schedule' => $arreglo['data_schedule'],
				'sucursal_id' => $arreglo['sucursal'],
				'vehicle_id' => $arreglo['vehicle_id'],
				'folio_CDA' => $arreglo['folio_CDA']

			);

			curl_setopt_array($curl, array(
			  CURLOPT_URL => "http://3.85.25.112/api/v1/schedule/",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => $datos,
			  CURLOPT_HTTPHEADER => array(
			    "Authorization: JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo0NCwidXNlcm5hbWUiOiJqZXN1cy5wYXpvc0BjZGFwZW5pbnN1bGEuY29tLm14IiwiZXhwIjoxNjEyNjI5NjYyLCJlbWFpbCI6Implc3VzLnBhem9zQGNkYXBlbmluc3VsYS5jb20ubXgifQ.xSA3Nyetvx1kFDHLuOQA1QeAoW_Pl_C0OXwi5XpoczE",
			    "Content-Type: multipart/form-data; boundary=--------------------------831099749274018518599904"
			  ),
			));

			$response = curl_exec($curl);

			curl_close($curl);
			//echo $response;
			//echo "<br>";
			if($err){
				return "cURL Error #:" . $err;
			} 
			else{
				return $response;
			}			
		}
	
	
		// public function mensaje_usuario( $Id_propietario_nube, $nombre_asesor, $Fecha_cita_tierra, $hora_cita_tierra, $mensaje  ){
		public function mensaje_usuario( $Id_propietario_nube, $Fecha_cita_tierra, $hora_cita_tierra, $mensaje  ){

			$interaccion_BPRO = new funciones_consultasBPRO();

			$curl = curl_init();
			// $mensaje = 'Su asesor es '.$nombre_asesor;
			//Este mensaje es de prueba para que veas el tipo de comentario
			$mensaje_base_titulo = utf8_encode('CITA/ORDEN: ');
			$titulo = $mensaje_base_titulo.$Fecha_cita_tierra.' a '.$hora_cita_tierra;
			

			curl_setopt_array($curl, array(
			  CURLOPT_URL => "http://3.85.25.112/api/v1/notifications/",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => array('title' => $titulo,
			  'message' => $mensaje,
			  'users' => $Id_propietario_nube),
			  CURLOPT_HTTPHEADER => array(
				"Authorization: JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6ImdvbnphbG8iLCJleHAiOjE2MDUwNDM0ODIsImVtYWlsIjoiZ29uemFsby5ydWl6QGRhY29kZXMuY29tLm14In0.XMqroKYOPqSmHJ25vvPySdXDwnqBFnFqsGlF-KjjQDg",
				"Content-Type: multipart/form-data; boundary=--------------------------416030304654188624448427"
			  ),
			));

			$response = curl_exec($curl);

			curl_close($curl);
			if($err){
				return "cURL Error #:" . $err;
			} 
			else{
				
				// si el mensaje se envi�, aqu� se bitacorear� en base de datos de que el mensaje fue enviado
				$interaccion_BPRO->bitacorea_app_mensajes($titulo,$mensaje,$Id_propietario_nube);
				
				return $response;
			}
		}
		
		public function actualiza_cita( $id_cita,  $id_cita_tierra, $hora_cita_tierra, $Fecha_cita_tierra, $estatus_cita ){
			/*			
			-------------------------------------------
			datos que pueden cambiar en una cita/orden
			-------------------------------------------
				  1 - FECHA
				  2 - HORA
				  3 - ESTATUS      // solamente estos 3 se actualizar�n los dem�s no
				  
				  4 - KILOMETRAJE ( esto cambiar�a en el auto , no en la cita )
				  5 - ASESOR      ( este es un tema pendiente al d�a de hoy 12-mar-2020 con DACODES, para que pongan un campo varchar y no num�rico )
			-------------------------------------------      			
			*/		
			
			$curl = curl_init();

			$FechaDividida = explode('/', $Fecha_cita_tierra);

			$dia = $FechaDividida[0];
			$mes = $FechaDividida[1];
			$anio = $FechaDividida[2];

			$fechaCorrecta = $anio."-".$mes."-".$dia;
			//echo $format = date("Y/m/d", strtotime($Fecha_cita_tierra))."<br>";

			$datos = array(
				"hour_schedule" => $hora_cita_tierra,
				"data_schedule" => $fechaCorrecta,//$Fecha_cita_tierra,
				"folio_CDA" => $id_cita_tierra,
				"status_id" => $estatus_cita
			);

			curl_setopt_array($curl, array(
		  		CURLOPT_URL => "http://3.85.25.112/api/v1/schedule/".$id_cita."/",
		  		CURLOPT_RETURNTRANSFER => true,
		  		CURLOPT_ENCODING => "",
		  		CURLOPT_MAXREDIRS => 10,
		  		CURLOPT_TIMEOUT => 0,
		  		CURLOPT_FOLLOWLOCATION => true,
		  		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  		CURLOPT_CUSTOMREQUEST => "PATCH",
		  		CURLOPT_POSTFIELDS => $datos,
		  		CURLOPT_HTTPHEADER => array(
	    								"Authorization: JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6ImdvbnphbG8iLCJleHAiOjE2MDUwNDM0ODIsImVtYWlsIjoiZ29uemFsby5ydWl6QGRhY29kZXMuY29tLm14In0.XMqroKYOPqSmHJ25vvPySdXDwnqBFnFqsGlF-KjjQDg",
	    								"Content-Type: multipart/form-data; boundary=--------------------------085622720903280288307670"
	    							)
				)
			);

			$response = curl_exec($curl);		

			curl_close($curl);

			if($err){
				return "cURL Error #:" . $err;
			} 
			else{
				return $response;
			}
		}		
		
		public function ConsultaCitaXVin($VIN){

			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => "http://3.85.25.112/api/v1/schedule/?vin=".$VIN,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_HTTPHEADER => array(
			    "Authorization: JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo0NCwidXNlcm5hbWUiOiJqZXN1cy5wYXpvc0BjZGFwZW5pbnN1bGEuY29tLm14IiwiZXhwIjoxNjEyNjI5NjYyLCJlbWFpbCI6Implc3VzLnBhem9zQGNkYXBlbmluc3VsYS5jb20ubXgifQ.xSA3Nyetvx1kFDHLuOQA1QeAoW_Pl_C0OXwi5XpoczE"
			  ),
			));

			$response = curl_exec($curl);

			$response = json_decode($response, true);
			curl_close($curl);
			$respuesta = $response['data'];
		
			if(empty($response['data'])){
				$respuesta = false;
			}
			
			//return $response['data'];
			return $respuesta;

		} 	
	}
?>